/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.tdd.ukcar.regnos;

import java.text.DecimalFormat;

/**
 *
 * @author Selvyn
 */
public  class UKRegNosHandler
{

    public  String nextRegCode(String initValue) throws BadYearException
    {
        String result = "";
        int regNo = Integer.parseInt(initValue);
        
        if (regNo > 50)
        {
            regNo -= 49;
            result = formatResult( regNo );
        } else
        {
            if (regNo == 50)
            {
                throw new BadYearException("2050 is an invalid year");
            } else
            {
                regNo += 50;
                result = formatResult(regNo);
            }
        }

        return result;
    }
    
    private String  formatResult( int value )
    {
        java.text.DecimalFormat nft = new DecimalFormat("#00.##");
        nft.setDecimalSeparatorAlwaysShown(false);
        return nft.format(value);
    }

}
