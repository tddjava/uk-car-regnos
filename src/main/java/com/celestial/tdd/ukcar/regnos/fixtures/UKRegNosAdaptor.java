/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.tdd.ukcar.regnos.fixtures;

import com.celestial.tdd.ukcar.regnos.BadYearException;
import com.celestial.tdd.ukcar.regnos.UKRegNosHandler;

/**
 *
 * @author Selvyn
 */
public class UKRegNosAdaptor
{
    private String  currenReg;
 
    public  UKRegNosAdaptor(){}
    
    public  void    setCurrentRegNo( String regNo )
    {
        currenReg = regNo;
    }
    
    public  String  NextRegNo() throws BadYearException 
    {
        UKRegNosHandler regHandler = new UKRegNosHandler();
        
        return regHandler.nextRegCode(currenReg);
    }
}
