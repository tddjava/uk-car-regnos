package com.celestial.ukcar.regnos.fx;

import com.celestial.tdd.ukcar.regnos.BadYearException;
import com.celestial.tdd.ukcar.regnos.UKRegNosHandler;

public class CarRegistrationData
{
    private String  currenReg;
    
    public	CarRegistrationData(){}
    
    public  void    setCurrentYearCode( String regNo )
    {
        currenReg = regNo;
    }
    
    public  String  NextYearCode() 
    {
        UKRegNosHandler regHandler = new UKRegNosHandler();
        
        try
        {
        	return regHandler.nextRegCode(currenReg);
        }
        catch(BadYearException e)
        {
        	return "invalid";
        }
    }

}
