/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.tdd.ukcar.regnos;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Selvyn
 */
public class UKRegNosHandlerTest
{

    public UKRegNosHandlerTest()
    {
    }

    @Test
    public void next_reg_no_after_march() throws BadYearException
    {
        //arrange - setup
        UKRegNosHandler regHandler = new UKRegNosHandler();
        String initValue = "15";
        String expValue = "65";

        // act
        String result = regHandler.nextRegCode(initValue);

        // assert
        assertThat(result, is(expValue));
    }

    @Test
    public void next_reg_no_after_september() throws BadYearException
    {
        //arrange - setup
        UKRegNosHandler regHandler = new UKRegNosHandler();
        String initValue = "65";
        String expValue = "16";

        // act
        String result = regHandler.nextRegCode(initValue);

        // assert
        assertThat(result, is(expValue));
    }

    @Test
    public void next_reg_no_after_march_in_noughties() throws BadYearException
    {
        //arrange - setup
        UKRegNosHandler regHandler = new UKRegNosHandler();
        String initValue = "02";
        String expValue = "52";

        // act
        String result = regHandler.nextRegCode(initValue);

        // assert
        assertThat(result, is(expValue));
    }

    @Test
    public void next_reg_no_after_september_in_noughties() throws BadYearException
    {
        //arrange - setup
        UKRegNosHandler regHandler = new UKRegNosHandler();
        String initValue = "51";
        String expValue = "02";

        // act
        String result = regHandler.nextRegCode(initValue);

        // assert
        assertThat(result, is(expValue));
    }

    @Test(expected = BadYearException.class)
    public void next_reg_no_after_march_in_2050() throws BadYearException
    {
        //arrange - setup
        UKRegNosHandler regHandler = new UKRegNosHandler();
        String initValue = "50";

        // act
        regHandler.nextRegCode(initValue);
    }

}
